.ONESHELL:
SHELL = /bin/bash


include settings.mk



.PHONY: all
all: backup git-circus backup create-issues backup



.PHONY: only-create-issues
only-create-issues: create-issues backup




###############################################################################
#### ISSUE MANAGEMENT:

.PHONY: create-issues
create-issues: $(DATA_DIRECTORY)/flawfinder.filtered.out $(DATA_DIRECTORY)/gitleaks.filtered.out $(ISSUEIFIED_HITS) $(DATA_DIRECTORY)/
	cat $(DATA_DIRECTORY)/flawfinder.filtered.out $(DATA_DIRECTORY)/gitleaks.filtered.out \
	| perl src/create-issues.pl --issueified $(ISSUEIFIED_HITS) \
		&& { rm -f $(DATA_DIRECTORY)/flawfinder.filtered.out.gz $(DATA_DIRECTORY)/gitleaks.filtered.out.gz \
	             ; gzip $(DATA_DIRECTORY)/flawfinder.filtered.out $(DATA_DIRECTORY)/gitleaks.filtered.out \
	             ; mv --backup=t $(DATA_DIRECTORY)/flawfinder.filtered.out.gz $(DATA_DIRECTORY)/flawfinder.filtered.out.gz.bak \
	             ; mv --backup=t $(DATA_DIRECTORY)/gitleaks.filtered.out.gz $(DATA_DIRECTORY)/gitleaks.filtered.out.gz.bak ; }



$(ISSUEIFIED_HITS):
	if [ ! -f $@ ]; then mkdir -p $(shell dirname $@) && touch $@; fi




###############################################################################
#### BACKUP:

.PHONY: backup
backup:
	$(BACKUP_CMD)




###############################################################################
#### CLONING:

.PHONY: git-circus
git-circus:
	cd $(GIT_CIRCUS_INSTALL_DIR) && make
