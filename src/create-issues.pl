#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;
use Text::CSV;
use v5.10;


GetOptions(
    'issueified=s' => \(my $file_issueified = '/tmp/dbfile_of_issueified_hits.txt'),
    'issue-report-repo-dir=s' =>  \(my $issue_report_repo_dir = glob("~/proj-af/security")),
) or die "Invalid options passed to $0\n";

print STDERR $ENV{PATH},"\n";
die("cannot find directory $issue_report_repo_dir") unless(-d $issue_report_repo_dir);
die("error running cd $issue_report_repo_dir && glab issue list") unless(system("cd $issue_report_repo_dir && glab issue list >/dev/null") == 0);



my $filetemplate = '# [](%BASEURL%/%REPOPATH%)

   - [ ] False detection - all below is not applicable

   In case of a true detection, populate below with X=done, n/a=not applicable.

   - [ ] I promise that I have REMOVED the commit with BFG, or MOVED the repo to a closed gitlab
   - [ ] I promise that I have CHANGED or REMOVED the secret in the REMOTE system
   - [ ] I promise that I have SAVED the new secret safely, not in a publicly reachable storage
   - [ ] I promise that I have VERIFIED that the application is still working in all environments with the updated secret

## Secret
```
%SECRET%
```

## Details
';


# slurp database
my %issueified_hits = ();
print STDERR "read database...\n";
map {
    my $l = normalise_string($_);
    $issueified_hits{$l} = 1
	unless($l =~ /^\s*#/);
} split(/\n/, read_file($file_issueified));



# read input data
my @indata = ();
print STDERR "read input...\n";
map {
    my $l = normalise_string($_);
    push(@indata, $l)
	unless($l =~ /^\s*#/);
} <STDIN>;
print STDERR "read ".@indata." lines\n";



# Process input lines
my %new_issues = ();
print STDERR "process input...\n";
foreach my $line (@indata)
{
    $line = normalise_string($line);
    if($line =~ /^"/) {
	if(! defined($issueified_hits{$line})) {
	    create_task(\%new_issues, $line);
	    $issueified_hits{$line} = 1;
	} else {
	    print(STDERR "skipping already issueified line ($line)\n");
	}
    }
}



# Create issues on Gitlab
print STDERR "process ".(keys %new_issues)." new issues...\n";
foreach my $key (keys %new_issues) {
    my $title = $new_issues{$key}->{'title'};
    my $tmpl = $new_issues{$key}->{'tmpl'};
    $tmpl .= '| commit link | type | detector |
| ---      | ---        | ---      |
';
    my $priolabel = 'priority::warning';

    foreach my $v (@{$new_issues{$key}->{'vuln'}}) {
	$tmpl .= sprintf("| [%s](%s) | %s | %s | %s |\n", $v->{'commit'}, $v->{'link'}, $v->{'detector'}, $v->{'type'}, $v->{'priolabel'});
	if($v->{'priolabel'} ne 'priority::warning') {
	    $priolabel = $v->{'priolabel'};
	}
    }

    chdir($issue_report_repo_dir) || die("cannot chdir $issue_report_repo_dir");
    die("cannot create issue: ".join(" ", ("glab", "issue", "create", "--confidential", "--yes", "--label", $priolabel, "--title", "$title", "--description", $tmpl)))
	unless(system("glab", "issue", "create", "--confidential", "--yes", "--label", $priolabel, "--title", "$title", "--description", $tmpl) == 0);
}


# Write updated issueified hits file
print STDERR "write updated hits file...\n";
open(my $fh0, '>', $file_issueified) || die("cannot write $file_issueified");
map {
    print($fh0 $_."\n");
} keys(%issueified_hits);
close($fh0);

print STDERR "ready.\n";
exit;


sub create_task {
    my $new_issues_ref = shift;
    my $line = shift;
    my $csv = Text::CSV->new({ binary => 1 });

    if($csv->parse($line)) {
	my ($detector, $link, $commit, $repopath, $baseurl, $filepath, $type, $secret, $priolabel) = $csv->fields();
	if($detector !~ /^\s*$/
	   && $link !~ /^\s*$/
	   && $commit !~ /^\s*$/
	   && $repopath !~ /^\s*$/
	   && $filepath !~ /^\s*$/
	   && $type !~ /^\s*$/
	   && $secret !~ /^\s*$/
	   && $priolabel !~ /^\s*$/) {
	    $secret =~ s/['"]//g;
	    my %repl = ( '%DETECTOR%' => $detector,
			 '%LINK%'     => "$baseurl/$repopath/-/blame/$commit/$filepath", #$link,
			 '%COMMIT%'   => $commit,
			 '%REPOPATH%' => $repopath,
			 '%FILEPATH%' => $filepath,
			 '%BASEURL%'  => $baseurl,
			 '%TYPE%'     => $type,
			 '%SECRET%'   => $secret,
			 '%LINE%'     => $line,
		);

	    if(! defined($new_issues_ref->{"$repopath/$filepath/$secret"})) {
		my $tmpl = $filetemplate;
		map { $tmpl =~ s/$_/$repl{$_}/g; } keys(%repl);
		$new_issues_ref->{"$repopath/$filepath/$secret"}->{"tmpl"} = $tmpl;
		$new_issues_ref->{"$repopath/$filepath/$secret"}->{"title"} = "$type in $repopath/$filepath";
	    }

	    push(@{$new_issues_ref->{"$repopath/$filepath/$secret"}->{"vuln"}},
		 { 'commit' => $commit, 'link' => $link, 'detector' => $detector, 'type' => $type, 'priolabel' => $priolabel });
	} else {
	    print(STDERR "bad fields: $detector,$link,$commit,$repopath,$filepath,$type,$secret,$priolabel\n");
	}
    } else {
	print(STDERR "could not parse line: $line (".$csv->error_diag().")\n");
    }
}



sub read_file {
  my $file = shift;
  open my $fh, '<', $file or die;
  local $/ = undef;
  my $cont = <$fh>;
  close $fh;
  return $cont;
}



sub normalise_string {
    chomp(my $l = $_[0]);
    $l =~ s/(^\s+|\s+$)//g;
    $l =~ s/\s+/ /g;
    return $l;
}
